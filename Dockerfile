ARG NODE_VERSION=erbium
FROM node:${NODE_VERSION}

WORKDIR /app

ADD package.json package-lock.json ./

RUN npm ci

ADD src/ ./src/

RUN ls -lah && npm run build

CMD npm run server
