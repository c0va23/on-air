import Vue from 'vue'
import App from './App.vue'

new Vue({ render: createElement => createElement(App) }).$mount('#app')

window.addEventListener('load', () => {
  const swPath = '/sw.js'
  navigator.serviceWorker.register(swPath)
    .then(console.info)
    .catch(console.error)
})
