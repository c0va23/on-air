import SocketIOClient from 'socket.io-client'

class DeviceStatusListener {
  /**
   * constructor
   * @param {string} machineId
   * @param {({video_enabled: boolean}) => void} deviceStatusCallback
   */
  constructor (machineId, deviceStatusCallback) {
    const socket = new SocketIOClient({
      query: {
        machineId: machineId,
      },
      transports: ['websocket'],
    })
    socket.on('date', (data) => {
      console.log(data)
    })
    socket.on('deviceStatusUpdated', (deviceStatus) => {
      deviceStatusCallback(deviceStatus)
      console.log(`Device updated ${JSON.stringify(deviceStatus)}`)
    })
  }
}

module.exports = DeviceStatusListener
