const EventsEmitter = require('events')

const eventName = 'deviceStatusUpdate'

class DeviceStatusEmitter {
  constructor (deviceStatusStore) {
    this._deviceStatusStore = deviceStatusStore
    this._emitter = new EventsEmitter()
  }

  /**
   * publish
   * @param {{
   *   machineId: string,
   *   videoEnabled: boolean,
   * }} deviceStatus
   */
  publish (deviceStatus) {
    this._deviceStatusStore
      .put(deviceStatus.machineId, deviceStatus)
      .then(() => this._emitter.emit(eventName, deviceStatus))
  }

  /**
   * subscribe
   * @param {string} machineId
   * @param {{
   *   machineId: string,
   *   videoEnabled: boolean,
   * } => void} callback
   */
  subscribe (machineId, callback) {
    console.debug(`Subscribe ${machineId}`)
    this._deviceStatusStore
      .get(machineId)
      .then((deviceStatus) => {
        if (deviceStatus) {
          console.debug(`Return from store ${JSON.stringify(deviceStatus)}`)
          callback(deviceStatus)
        } else {
          console.warn(`Not found into store ${machineId}`)
        }
      })
      .finally(() => {
        this._emitter.addListener(eventName, callback)
      })
  }

  /**
   * subscribe
   * @param {{
   *   machineId: string,
   *   videoEnabled: boolean,
   * } => void} callback
   */
  unsubscribe (callback) {
    console.debug('Unsubscribe')
    this._emitter.removeListener(eventName, callback)
  }
}

module.exports = DeviceStatusEmitter
