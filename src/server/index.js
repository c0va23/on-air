const express = require('express')
const http = require('http')
const socketIO = require('socket.io')
const MemStore = require('./deviceStatusStore/memStore')
const DeviceStatusEmitter = require('./DeviceStatusEmitter')

const app = express()

const memStore = new MemStore()
const deviceStatusEmitter = new DeviceStatusEmitter(memStore)

const deviceStatusHandler = require('./handlers/deviceStatusHandler')

app.use(express.json())

app.post('/device-status', deviceStatusHandler(deviceStatusEmitter))

const bundler = require('./bundler')
app.use(bundler)

const DEFAULT_PORT = 9999
const port = parseInt(process.env.PORT) || DEFAULT_PORT

const DEFAULT_BIND = '0.0.0.0'
const bind = process.env.BIND || DEFAULT_BIND

console.log(`Start listen ${bind}:${port}`)
const httpServer = http.createServer({}, app)
httpServer.listen(port, bind)

const io = socketIO(httpServer, {
  transports: ['websocket'],
})

const socketHandler = require('./socketHandler')

io.on('connect', socketHandler(deviceStatusEmitter))
