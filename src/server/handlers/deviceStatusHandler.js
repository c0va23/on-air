// const express = require('express')

/**
 * deviceStatusReceiverHandler
 * @param {express.Request} req
 * @param {express.Response} res
 */
const deviceStatusReceiverHandler = (eventEmitter) => (req, res) => {
  console.log(`Device status ${JSON.stringify(req.body)}`)
  eventEmitter.publish(req.body)
  res.sendStatus(200)
}

module.exports = deviceStatusReceiverHandler
