class MemStore {
  constructor () {
    this._store = {}
  }

  put (machineId, deviceStatus) {
    this._store[machineId] = deviceStatus
    return Promise.resolve()
  }

  get (machineId) {
    const deviceStatus = this._store[machineId]
    return Promise.resolve(deviceStatus)
  }

  delete (machineId) {
    delete this._store[machineId]
    return Promise.resolve()
  }
}

module.exports = MemStore
