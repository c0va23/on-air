/**
  * @param {DeviceStatusEmitter} deviceStatusEmitter
  */
function socketHandler (deviceStatusEmitter) {
  return function (socket) {
    console.log('New connection')
    const machineId = socket.handshake.query.machineId
    console.log(`Machine ID ${machineId}`)
    if (!machineId) {
      console.error('Machine id not found')
      return
    }

    const emmitCallback = (deviceStatus) => {
      if (machineId !== deviceStatus.machineId) return
      console.debug(`Send ${JSON.stringify(deviceStatus)}`)
      socket.emit('deviceStatusUpdated', deviceStatus)
    }

    deviceStatusEmitter.subscribe(machineId, emmitCallback)
    socket.on('disconnect', () => {
      deviceStatusEmitter.unsubscribe(emmitCallback)
    })
  }
}

module.exports = socketHandler
