const staticDirPath = './dist'

if (process.env.NODE_ENV === 'production') {
  const express = require('express')

  module.exports = express.static(staticDirPath)
} else {
  const Bundler = require('parcel-bundler')

  const bundler = new Bundler('src/front/index.html', {
    hmr: true,
    outDir: staticDirPath,
  })

  module.exports = bundler.middleware()
}
